package dwfe.modules.common;

import dwfe.test.DwfeTestChecker;

import java.util.List;
import java.util.Map;

public class CommonTestVariablesForApiTest
{
  public static final List<DwfeTestChecker> checkers_for_googleCaptchaValidate = List.of(
          DwfeTestChecker.of(false, 200, Map.of(), "missing-google-captcha"),
          DwfeTestChecker.of(false, 200, Map.of("googleResponse", ""), "empty-google-captcha"),
          DwfeTestChecker.of(false, 200, Map.of("googleResponse", "12345"), "google-captcha-detected-robot")
  );
}
