package dwfe.modules.common;

import dwfe.config.DwfeConfigProperties;
import dwfe.modules.common.config.CommonConfigProperties;
import dwfe.test.config.DwfeTestConfigProperties;
import dwfe.test.config.DwfeTestLevelAuthority;
import dwfe.test.config.DwfeTestVariablesForAuthAccessTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashMap;
import java.util.Map;

import static dwfe.test.config.DwfeTestLevelAuthority.ANY;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Component
public class CommonTestVariablesForAuthAccessTest extends DwfeTestVariablesForAuthAccessTest
{
  @Autowired
  private DwfeConfigProperties propDwfe;
  @Autowired
  private DwfeTestConfigProperties propDwfeTest;
  @Autowired
  private CommonConfigProperties propCommon;

  @Override
  public Map<String, Map<DwfeTestLevelAuthority, Map<RequestMethod, Map<String, Object>>>> RESOURCE_AUTHORITY_reqDATA()
  {
    var commonUri = propDwfe.getService().getGatewayUrl() + propDwfe.getService().getCommon().getGatewayPath();
    Map<String, Map<DwfeTestLevelAuthority, Map<RequestMethod, Map<String, Object>>>> result = new HashMap<>();

    // DWFE
    result.put(commonUri + propCommon.getResource().getGoogleCaptchaValidate(), Map.of(ANY, Map.of(POST, Map.of())));

    return result;
  }
}
