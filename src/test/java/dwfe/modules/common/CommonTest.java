package dwfe.modules.common;

import dwfe.config.DwfeConfigProperties;
import dwfe.modules.common.config.CommonConfigProperties;
import dwfe.test.DwfeTestAuth;
import dwfe.test.DwfeTestUtil;
import dwfe.test.config.DwfeTestConfigProperties;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static dwfe.modules.common.CommonTestVariablesForApiTest.checkers_for_googleCaptchaValidate;
import static dwfe.test.DwfeTestUtil.pleaseWait;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

//
// == https://spring.io/guides/gs/testing-web/
//

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT  // == https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html#boot-features-testing-spring-boot-applications
)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CommonTest
{
  private static final Logger log = LoggerFactory.getLogger(CommonTest.class);

  @Autowired
  private DwfeConfigProperties propDwfe;
  @Autowired
  private DwfeTestConfigProperties propDwfeTest;
  @Autowired
  private CommonConfigProperties propCommon;
  @Autowired
  private DwfeTestUtil utilTest;
  @Autowired
  private DwfeTestAuth auth;


  //-------------------------------------------------------
  // ACTUATOR
  //

  @Test
  public void _00_01_actuator()
  {
    pleaseWait(65, log);
    utilTest.test_actuator();
  }


  //-------------------------------------------------------
  // COMMON
  //

  @Test
  public void _01_01_googleCaptchaValidate()
  {
    var resource = propCommon.getResource().getGoogleCaptchaValidate();
    utilTest.check(commonApiRoot() + resource, POST, auth.getAnonym_accessToken(), checkers_for_googleCaptchaValidate);
  }


  //-------------------------------------------------------
  // UTILs
  //

  private String commonApiRoot()
  {
    return propDwfe.getService().getGatewayUrl() + propDwfe.getService().getCommon().getGatewayPath();
  }

}
